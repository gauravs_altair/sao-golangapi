package utilities

import (
	"github.com/magiconair/properties"
)

func ReadConfig(filename string) map[string]string {

	// get properties
	p := properties.MustLoadFile(filename, properties.UTF8)

	// get values through getters
	host := p.MustGetString("db.host")
	port := p.MustGetString("db.port")
	user := p.MustGetString("db.user")
	password := p.MustGetString("db.password")
	dbname := p.MustGetString("db.database")
	dbschema := p.MustGetString("db.schema")
	https_port := ":" + p.MustGetString("server.port")

	x := make(map[string]string)

	x["host"] = host
	x["port"] = port
	x["user"] = user
	x["password"] = password
	x["dbname"] = dbname
	x["dbschema"] = dbschema
	x["https_port"] = https_port

	return x
}
