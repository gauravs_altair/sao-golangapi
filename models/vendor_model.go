package models

type VendorDetails struct {
	Id     string `json:"id_vendormain"`
	Name   string `json:"vendor_alias"`
	Method string `json:"vendor_method"`
	Server string `json:"servers_key"`
	Port   string `json:"vendor_port"`
	Status string `json:"vendor_status"`
}
