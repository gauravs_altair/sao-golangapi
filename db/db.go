package db

import (
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Connect(host string, port string, user string, password string, dbname string, dbschema string) *gorm.DB {

	dsn := "host=" + host + " user=" + user + " password=" + password + " dbname=" + dbname + " port=" + port + " search_path=" + dbschema + " sslmode=disable TimeZone=GMT"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	//psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable search_path=%s", host, port, user, password, dbname, dbschema)
	//db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("DB Connected...")
	}

	return db
}
