package config

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	//host     = "altairsao3.prog.altair.com"
	//port     = 5432
	host     = "localhost"
	port     = 5433
	user     = "postgres"
	password = "postgres"
	dbname   = "sao"
	dbschema = "saoschema"
)

func ConnectDB() *sql.DB {
	// host := "altairsao3.prog.altair.com"
	// port := "5432"
	// user := "postgres"
	// password := "postgres"
	// dbname := "sao"

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable search_path=%s", host, port, user, password, dbname, dbschema)
	db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	} else {
		fmt.Println("DB Connected...")
	}

	return db
}
