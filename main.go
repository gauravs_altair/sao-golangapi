package main

import (
	"fmt"
	"go_psql/db"
	"go_psql/models"
	"go_psql/utilities"
	"net/http"

	"github.com/labstack/echo/v4"

	_ "github.com/lib/pq"
)

func main() {

	// get properties
	pmap := utilities.ReadConfig("application.properties")

	// connect to db
	db := db.Connect(pmap["host"], pmap["port"], pmap["user"], pmap["password"], pmap["dbname"], pmap["dbschema"])

	e := echo.New()

	e.GET("/vendor", func(c echo.Context) error {

		fmt.Println("list of vendors")

		var vendor []models.VendorDetails

		stmt := "select id_vendormain AS id, vendor_alias as name, vendor_method as method, servers_key as server, vendor_port as port, vendor_status as status from sao_vendormain order by vendor_alias"
		fmt.Println(stmt)

		rows, err := db.Raw(stmt).Rows()
		defer rows.Close()
		if err != nil {
			fmt.Print(err)
		}

		var (
			id     string
			name   string
			method string
			server string
			port   string
			status string
		)
		for rows.Next() {
			rows.Scan(&id, &name, &method, &server, &port, &status)
			fmt.Println(id + " " + name + " " + method + " " + server + " " + port + " " + status)
			vendor = append(vendor, models.VendorDetails{
				Id: id, Name: name, Method: method, Server: server, Port: port, Status: status,
			})
		}
		return c.JSON(http.StatusOK, vendor)
	})
	e.Logger.Fatal(e.Start(pmap["https_port"]))

}

func oldfunc() {

	// get properties
	// pmap := utilities.ReadConfig("application.properties")

	// // connect to db
	// db := db.Connect(pmap["host"], pmap["port"], pmap["user"], pmap["password"], pmap["dbname"], pmap["dbschema"])

	// e := echo.New()

	// e.GET("/vendor", func(c echo.Context) error {

	// 	fmt.Print("list of vendors")
	// 	sqlStatement := "select id_vendormain, vendor_alias, vendor_method, servers_key, vendor_port, vendor_status from sao_vendormain"

	// 	fmt.Print(sqlStatement)

	// 	res, err := db.Query(sqlStatement)
	// 	if err != nil {
	// 		fmt.Print(err)
	// 	}

	// 	defer res.Close()
	// 	for res.Next() {
	// 		var (
	// 			data_start     string
	// 			data_end       string
	// 			id_featuremain int
	// 			users          string
	// 		)
	// 		if err := res.Scan(&data_start, &data_end, &id_featuremain, &users); err != nil {
	// 			panic(err)
	// 		}
	// 		fmt.Printf("%s %s %d %s\n", data_start, data_end, id_featuremain, users)
	// 	}
	// 	if err := res.Err(); err != nil {
	// 		panic(err)
	// 	}

	// 	return c.JSON(http.StatusOK, res)
	// })
	// e.Logger.Fatal(e.Start(pmap["https_port"]))

}
